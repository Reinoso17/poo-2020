package edu.uni.poo.lab.s01;

import java.util.Scanner;

public class Problema02 {
    public static int calcularsuma(int n) {
        int suma = 0;
        for (int i = 1; i <= n; i++) {
            suma = suma + i;
            System.out.println(i);
        }
        return suma;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("ingrese valor:");
        int n = sc.nextInt();

        int suma = calcularsuma(n);
        System.out.println("la suma es:" + suma);
    }

}

