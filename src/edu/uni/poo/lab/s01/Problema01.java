package edu.uni.poo.lab.s01;

import java.util.Scanner;

public class Problema01 {
    public static float obtenerpromedio(int a, int b) {
        float promedio = (a + b) / 2.0f;
        return promedio;

    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("digite el valor de a:");
        int valor1 = sc.nextInt();
        System.out.println("digite el valor de b:");
        int valor2 = sc.nextInt();
        float promedio = obtenerpromedio(valor1, valor2);
        System.out.println("el promedio es:" + promedio);
    }

}


